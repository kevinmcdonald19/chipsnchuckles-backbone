console.log('getting here');

$(document).ready(function () {
    console.log('dom is loaded');

    // Models
    var User = Backbone.Model.extend({
        // the locaiton of the RESPONSE from the server
        urlRoot: './server/users'
    });

    var Absence = Backbone.Model.extend({
        urlRoot: './server/absences'
    });


    // Views
    var LoginView = Backbone.View.extend({
        el: '.page',
        render: function (options) {
            var template = _.template($('#loginTemplate').html());
            this.$el.html(template);
        },
        events: {
            "click #btn_login": "login"
        },

        login: function () {
            console.log('attempt to login');

            var formData = $('#loginForm').serializeObject();

            var user = new User();
            var self = this;
            user.save(formData, {
                success: function (msg) {
                    console.log('saved user');

                    // navigate to time enter screen
                    //self.$el.fadeOut(200);
                    router.navigate('entry/' + msg.id, {trigger: true});

                },
                error: function () {
                    console.log('error saving');
                }
            });


            // prevent default submit
            return false;
        }
    });

    var EntryView = Backbone.View.extend({
        el: '.page',
        render: function (options) {
            this.id = options.id;
            var template = _.template($('#entryTemplate').html());
            this.$el.html(template).fadeIn();
        },

        events: {
            "submit": "submitAbsence",
            "click #cancel": "logout"
        },

        logout: function (event) {
            console.log('logout');
            router.navigate("", {trigger: true});
        },

        submitAbsence: function (event) {
            console.log('submit absence');

            var formData = $('#absenceForm').serializeObject();

            // todo make this a colleciton so you can submit multiples
//            user.set('absence', new Absence(formData));
            var self = this;

            var absence = new Absence();
//            absence.set('absenceId', this.id);
            absence.save(formData, {
                success: function (msg) {
                    console.log('success submitting to server');
                    router.navigate("success",  {trigger: true});
                },

                error: function (msg) {
                    console.log('error submitting absence' + msg);
                }
            });

            event.preventDefault();
        }
    });

    var SuccessView = Backbone.View.extend({
        el: '.page',
        render: function () {
            console.log('success view render');
            var template = _.template($('#successTemplate').html());
            this.$el.html(template);
        },

        events: {
            "click #homeBtn": "goHome"
        },

        goHome: function () {
            console.log('go home');
            router.navigate('', {trigger: true});
            return false;
        }
    });
    // Router
    var Router = Backbone.Router.extend({
        routes: {
            "": "home",
            "entry/:id": "newEntry",
            "success": "successPage"
        }
    });

    // Instantiations
//    userModel = new User();
//    userModel.fetch({
//        success: function(){
//            console.log('successful load from static server');
//        },
//        error:function(err){
//            console.log('failed to call');
//        }
//    });
    var loginView = new LoginView();
    var entryView = new EntryView();
    var successView = new SuccessView();
    var router = new Router;
    router.on('route:home', function () {
        console.log('loading home');
        loginView.render();
    });

    router.on('route:newEntry', function (id) {
        console.log('loading entry page' + id);
        entryView.render({id: id});

    });

    router.on('route:successPage', function () {
        console.log('success route');
        successView.render();
    });
    Backbone.history.start();

});